package com.example.servicedemo

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

const val TAG = "Service demo"

class MainActivity : AppCompatActivity() {
    private lateinit var btnStart: Button
    private lateinit var btnStop: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnStart = findViewById(R.id.startButton)
        btnStop = findViewById(R.id.stopButton)

        btnStart.setOnClickListener {
            startService(Intent(this,MyService::class.java))

        }

        btnStop.setOnClickListener {
            stopService(Intent(this,MyService::class.java))

        }
    }

}