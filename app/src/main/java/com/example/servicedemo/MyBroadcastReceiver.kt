package com.example.servicedemo

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast

class MyBroadcastReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d(TAG, "onReceive: ")
        if (intent?.action == "com.example.MY_BROADCAST") {
            val message = intent.getStringExtra("message")
            Toast.makeText(context, " Received message: $message", Toast.LENGTH_LONG).show()
            Log.d(TAG, "Received message: $message")
        }
    }
}