package com.example.servicedemo

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import android.util.Log
import android.widget.Toast

class MyService : Service() {

    private val myBroadcastReceiver =  MyBroadcastReceiver()

    override fun onBind(intent: Intent): IBinder? {
        Log.d(TAG, "onBind: ")
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText( this,"service start ", Toast.LENGTH_SHORT).show()

        sendBroadcast()

        Log.d(TAG, "onStartCommand: ")
        return START_STICKY
    }

    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    fun sendBroadcast(){
        val intentFilter = IntentFilter("com.example.MY_BROADCAST")
        registerReceiver(myBroadcastReceiver,intentFilter)
        val intent = Intent("com.example.MY_BROADCAST")
        intent.putExtra("message", "Hello from the service!")
        sendBroadcast(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText( this,"service stop Broadcast unregister", Toast.LENGTH_SHORT).show()

        unregisterReceiver(myBroadcastReceiver)
        Log.d(TAG, "onDestroy: ")

    }
}